%global pypi_name pyRXP
%global pypi_version 3.0.0

Name:    python-%{pypi_name}
Version: %{pypi_version}
Release: 1%{?dist}
Summary: Python RXP interface - fast validating XML parser
License: BSD
URL:     http://www.reportlab.com
Source0: %{pypi_source}

BuildRequires: gcc
BuildRequires: python3-devel
BuildRequires: python%{python3_pkgversion}-setuptools

# test requirements
BuildRequires: python%{python3_pkgversion}-psutil

%description
Python RXP interface - fast validating XML parser

%package -n python%{python3_pkgversion}-%{pypi_name}
Summary: %{summary}
%{?python_provide:%python_provide python%{python3_pkgversion}-%{pypi_name}}
%description -n python%{python3_pkgversion}-%{pypi_name}
Python %{python3_version} RXP library

%prep
%autosetup -n %{pypi_name}-%{pypi_version}

%build
%py3_build

%install
%py3_install

%check
export PYTHONPATH="%{buildroot}%{python3_sitearch}"
%{__python3} setup.py test

%files -n python%{python3_pkgversion}-%{pypi_name}
%license LICENSE.txt
%doc README.rst
%{python3_sitearch}/%{pypi_name}*

%changelog
* Fri Dec 10 2021 Robert Bruntz/Duncan Macleod - 3.0.0-1
- Initial package based on PyPI release
